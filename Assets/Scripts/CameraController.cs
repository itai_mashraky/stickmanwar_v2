using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float Speed = 5f;

    private float Horizontal;
    private float Vertical;

    private Vector3 newPos;

    // Start is called before the first frame update
    void Start()
    {
        newPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Horizontal = Input.GetAxis("Horizontal");
        Vertical = Input.GetAxis("Vertical");

        newPos += new Vector3(Horizontal, -Vertical, Vertical) * Speed * Time.deltaTime;

        newPos.x = Mathf.Clamp(newPos.x, -40f, 40f);
        newPos.y = Mathf.Clamp(newPos.y, 4f, 9f);
        newPos.z = Mathf.Clamp(newPos.z, -15f, -7f);

        transform.position = newPos;
    }
}
