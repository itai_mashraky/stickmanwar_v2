using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WorkerController : MonoBehaviour
{

    [SerializeField] private NavMeshAgent Worker;

    [SerializeField] private Vector3 Target;

    [SerializeField] private Vector3 Base;

    private bool DestinationState = true;

    private bool AbleToChop = false;

    private float TimeBetweenChops = 0;

    [SerializeField] private float timerReset;
    [SerializeField] private bool IsRed;
    [SerializeField] private int InsideBag = 0;
    [SerializeField] private int MaxCarry = 100;
    [SerializeField] private Base_Controller Base_Controller_Script;

    // Start is called before the first frame update
    void Start()
    {
        if (IsRed)
        {
            Base_Controller_Script = GameObject.Find("Red_Base_Statue").GetComponent<Base_Controller>();
        }
        else
        {
            Base_Controller_Script = GameObject.Find("Blue_Base_Statue").GetComponent<Base_Controller>();
        }

        FindNearestTree();

        Worker = gameObject.GetComponent<NavMeshAgent>();

        Worker.SetDestination(Target);

        Base = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        if (Worker.remainingDistance <= 1.5f && DestinationState && !AbleToChop)
        {
            SetChopState();
        }

        if (Worker.remainingDistance <= 1.5f && !DestinationState)
        {
            Base_Controller_Script.AddMoney(InsideBag);
            InsideBag = 0;
            ChangeDest();
        }

        if (AbleToChop && TimeBetweenChops > timerReset)
        {
            InsideBag += 10;
            TimeBetweenChops = 0;
        }

        if (InsideBag >= MaxCarry && AbleToChop)
        {
            SetChopState();
            ChangeDest();
        }

        TimeBetweenChops += Time.deltaTime;

    }

    public void SetChopState()
    {
        AbleToChop = !AbleToChop;
    }

    public void ChangeDest()
    {
        if (DestinationState)
        {
            Worker.SetDestination(Base);
        }
        else
        {
            Worker.SetDestination(Target);
        }

        DestinationState = !DestinationState;

    }


    private void FindNearestTree()
    {
        GameObject[] Trees;
        if (IsRed)
        {
            Trees = GameObject.FindGameObjectsWithTag("Red_Tree");

        }
        else
        {
            Trees = GameObject.FindGameObjectsWithTag("Blue_Tree");
        }


        Target = Trees[UnityEngine.Random.Range(0, Trees.Length)].transform.position;


    }


}
