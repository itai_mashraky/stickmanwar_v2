using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Soldier_Trigger_Zone : MonoBehaviour
{

    [SerializeField] private Soldier_Controller Soldier_Parent;

    private void Start()
    {
        Soldier_Parent = gameObject.GetComponentInParent<Soldier_Controller>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!Soldier_Parent.getAtEnemyBase())
        {
            if (other.CompareTag("Soldier"))
            {
                if (Soldier_Parent.GetIsRed())
                {
                    if (!other.GetComponent<Soldier_Controller>().GetIsRed())
                    {
                        Soldier_Parent.SetTarget(other.gameObject.transform);
                    }
                }
                else
                {
                    if (other.GetComponent<Soldier_Controller>().GetIsRed())
                    {
                        Soldier_Parent.SetTarget(other.gameObject.transform);
                    }
                }
            }
            else if (other.CompareTag("Base"))
            {
                if (Soldier_Parent.GetIsRed())
                {
                    if (!other.GetComponent<Base_Controller>().GetIsRed())
                    {
                        Soldier_Parent.ArrivedAtEnemyBase();
                    }
                }
                else
                {
                    if (other.GetComponent<Base_Controller>().GetIsRed())
                    {
                        Soldier_Parent.ArrivedAtEnemyBase();
                    }
                }

            }
        }
    }

}
