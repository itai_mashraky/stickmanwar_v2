using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Soldier_Controller : MonoBehaviour
{
    [SerializeField] private Transform EnemyBase, CurrenttTarget;
    [SerializeField] private NavMeshAgent SoldierAgent;
    [SerializeField] private bool isRed, AttackMode, AtEnemyBase;
    [SerializeField] private float counter, counterReset = 5;
    [SerializeField] private int HP, Damage;
    [SerializeField] private AI_Controller AI_Controller_Script;
    [SerializeField] private HealthBar healthBar;

    // Start is called before the first frame update
    void Start()
    {
        healthBar.SetMaxHealth(HP);
        SoldierAgent = GetComponent<NavMeshAgent>();
        AI_Controller_Script = GameObject.Find("Blue_Base_Statue").GetComponent<AI_Controller>();

        if (isRed)
        {
            AI_Controller_Script.SetEnemySoldierCount(1);
            EnemyBase = GameObject.Find("Blue_Base_Statue").transform;
        }
        else
        {
            AI_Controller_Script.Set_AI_SoldierCount(1);
            EnemyBase = GameObject.Find("Red_Base_Statue").transform;
        }

        CurrenttTarget = EnemyBase;

        HP = 100;
    }

    // Update is called once per frame
    void Update()
    {
        if (!AtEnemyBase)
        {
            if (!AttackMode && CurrenttTarget != null)
            {
                SoldierAgent.SetDestination(CurrenttTarget.position);
            }

            if (CurrenttTarget != EnemyBase && SoldierAgent.remainingDistance <= 1f)
            {
                SoldierAgent.SetDestination(transform.position);
                GetComponent<Rigidbody>().velocity = Vector3.zero;
                GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

                AttackMode = true;

            }

            if (AttackMode && counter <= 0)
            {
                counter = counterReset;
                CurrenttTarget.gameObject.GetComponent<Soldier_Controller>().TakeDamage(Damage);
            }

            if (CurrenttTarget == null)
            {
                AttackMode = false;
                SetTarget(EnemyBase);
            }

        }
        else if (SoldierAgent.remainingDistance <= 1f)
        {
            SoldierAgent.SetDestination(transform.position);
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

            if (counter <= 0)
            {
                counter = counterReset;
                CurrenttTarget.gameObject.GetComponent<Base_Controller>().TakeDamage(Damage);
            }


        }


        if (HP <= 0)
        {
            if (isRed)
            {
                AI_Controller_Script.SetEnemySoldierCount(-1);
            }
            else
            {
                AI_Controller_Script.Set_AI_SoldierCount(-1);
            }

            Destroy(gameObject);
        }
        counter -= Time.deltaTime;
    }

    public void TakeDamage(int damage)
    {
        HP -= damage;
        healthBar.SetHealth(-damage);
    }

    public bool GetIsRed()
    {
        return isRed;
    }

    public void SetTarget(Transform target)
    {
        CurrenttTarget = target;
    }

    public void ArrivedAtEnemyBase()
    {
        AtEnemyBase = true;
    }
    public bool getAtEnemyBase()
    {
        return this.AtEnemyBase;
    }
}
