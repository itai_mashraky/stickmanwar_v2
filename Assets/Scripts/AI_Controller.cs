using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Controller : MonoBehaviour
{
    public enum State
    {
        start,    //0
        attack    //2
    }

    [SerializeField] private State ActiveState;
    [SerializeField] private int RequiredWorkerAmount, WorkerAmount, EnemySoldierCount, AI_SoldierCount, RequiredMoneyAmount;
    [SerializeField] private Base_Controller AI_Base_Controller_script;
    [SerializeField] private float Counter, CounterReset;

    // Start is called before the first frame update
    void Start()
    {

        AI_Base_Controller_script = gameObject.GetComponent<Base_Controller>();
        RequiredWorkerAmount = Random.Range(3, 6);
        RequiredMoneyAmount = Random.Range(800, 1201);
    }

    // Update is called once per frame
    void Update()
    {
        if (ActiveState != State.attack && EnemySoldierCount > AI_SoldierCount && AI_Base_Controller_script.GetMoney() > 100)
        {
            AI_Base_Controller_script.CreateSoldier();
        }

        switch (ActiveState)
        {

            case State.start:
                if (RequiredWorkerAmount > WorkerAmount)
                {
                    if (AI_Base_Controller_script.GetMoney() > 50 && Counter <= 0)
                    {
                        WorkerAmount++;
                        AI_Base_Controller_script.CreateWorker();
                        Counter = CounterReset;
                    }
                }
                else
                {
                    ActiveState = State.attack;
                }
                break;
            case State.attack:
                if (AI_Base_Controller_script.GetMoney() > RequiredMoneyAmount)
                {
                    if (AI_Base_Controller_script.GetMoney() >= 100 && Counter <= 0 && AI_SoldierCount <= 7)
                    {
                        AI_Base_Controller_script.CreateSoldier();
                        Counter = CounterReset;
                    }
                }
                break;
        }
        Counter -= Time.deltaTime;
    }

    public void SetEnemySoldierCount(int value)
    {
        EnemySoldierCount += value;
    }
    public void Set_AI_SoldierCount(int value)
    {
        AI_SoldierCount += value;
    }

    public int GetEnemySoldierCount()
    {
        return this.EnemySoldierCount;
    }
}
