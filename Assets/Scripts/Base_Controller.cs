using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Base_Controller : MonoBehaviour
{

    [SerializeField] private HealthBar healthBar;
    [SerializeField] private GameObject WorkerPrefab;

    [SerializeField] private Transform WorkerSpawn;

    [SerializeField] private Transform SoldierSpawn;

    [SerializeField] private GameObject SoldierPrefab, WorkerButton, SoldierButton, MoneyDisplay;

    [SerializeField] private bool IsRed;

    [SerializeField] private int HP, money, workerAmount, SoldierAmount;

    [SerializeField] private float Counter, CounterReset;
    [SerializeField] private UI_Manager UI_Manager_Script;
    [SerializeField] private AI_Controller AI_Controller_Script;


    // Start is called before the first frame update
    void Start()
    {
        Counter = CounterReset;
        UI_Manager_Script = GameObject.Find("Canvas").GetComponent<UI_Manager>();
        SoldierButton = GameObject.Find("Soldier_Spawn_Button");
        WorkerButton = GameObject.Find("Worker_Spawn_Button");
        MoneyDisplay = GameObject.Find("Money_Display");
        healthBar.SetMaxHealth(HP);
    }

    // Update is called once per frame
    void Update()
    {
        if (IsRed)
        {
            if (AI_Controller_Script.GetEnemySoldierCount() >= 7)
            {
                SoldierButton.GetComponent<Button>().enabled = false;
                SoldierButton.GetComponentInChildren<Text>().text = "Max Soldier Achived";

            }
            else
            {
                if (money < 100)
                {
                    SoldierButton.GetComponent<Button>().enabled = false;
                    SoldierButton.GetComponentInChildren<Text>().text = "Insufficient funds.";

                }
                else
                {
                    SoldierButton.GetComponent<Button>().enabled = true;
                    SoldierButton.GetComponentInChildren<Text>().text = "Soldier - 100$";

                }

            }
            if (workerAmount >= 5)
            {
                WorkerButton.GetComponent<Button>().enabled = false;
                WorkerButton.GetComponentInChildren<Text>().text = "Max Worker Achieved.";

            }
            else
            {
                if (money < 50)
                {
                    WorkerButton.GetComponent<Button>().enabled = false;
                    WorkerButton.GetComponentInChildren<Text>().text = "Insufficient funds.";
                }
                else
                {
                    WorkerButton.GetComponent<Button>().enabled = true;
                    WorkerButton.GetComponentInChildren<Text>().text = "Worker - 50$";

                }

            }
            MoneyDisplay.GetComponent<Text>().text = "Money Amount: " + money + ".";

            if (HP <= 0)
            {
                UI_Manager_Script.LoseGame();
            }
        }
        else
        {
            if (HP <= 0)
            {
                UI_Manager_Script.WinGame();
            }

        }
        if (Counter <= 0)
        {
            money += 50;
            Counter = CounterReset;
        }

        Counter -= Time.deltaTime;
    }

    public void CreateWorker()
    {
        money -= 50;
        Instantiate(WorkerPrefab, WorkerSpawn.position, Quaternion.identity);
        workerAmount++;
    }

    public void CreateSoldier()
    {
        money -= 100;
        Instantiate(SoldierPrefab, SoldierSpawn.position, Quaternion.identity);
    }

    public void TakeDamage(int Damage)
    {
        HP -= Damage;
        healthBar.SetHealth(-Damage);
    }

    public bool GetIsRed()
    {
        return IsRed;
    }

    public void AddMoney(int value)
    {
        this.money += value;
    }

    public int GetMoney()
    {
        return this.money;
    }
}
