using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_Manager : MonoBehaviour
{
    [SerializeField] private GameObject PauseMenu, WinMenu, LoseMenu;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            PauseGame();
        }
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        PauseMenu.SetActive(true);
    }

    public void ResumeGame()
    {
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
    }

    public void RestartGame()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        print("Main menu");
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
    }
    public void LoseGame()
    {
        Time.timeScale = 0;
        LoseMenu.SetActive(true);
    }
    public void WinGame()
    {
        Time.timeScale = 0;
        WinMenu.SetActive(true);
    }

}
