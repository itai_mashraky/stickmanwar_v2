using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Slider Slider;
    [SerializeField] private Gradient Color;
    [SerializeField] private Image Fill;

    public void SetMaxHealth(int maxHealth)
    {
        Slider.maxValue = maxHealth;
        Slider.value = maxHealth;

        Fill.color = Color.Evaluate(1f);
    }

    public void SetHealth(int value)
    {
        Slider.value += value;
        Fill.color = Color.Evaluate(Slider.normalizedValue);
    }

}
